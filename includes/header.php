<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">
<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Example</title>
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<?php /*
	<link rel="icon" href="img/favicon.ico">
	<link rel="canonical" href="http://www.example.com/">
	<meta name="keywords" content="keywords">
	<meta name="description" content="description">
	<meta name="author" content="author">
	<!-- place -->
	<meta name="geo.region" content="LT-VL">
	<meta name="ICBM" content="54.687156, 25.279651">
	<meta name="geo.placename" content="Vilnius">
	<meta name="geo.position" content="54.687156;25.279651">
	<meta name="dc.language" content="lt">
	<!-- itemprop -->
	<meta itemprop="name" content="Example">
	<meta itemprop="description" content="description">
	<meta itemprop="image" content="http://www.example.com/images/example.jpg">
	<!-- image -->
	<meta property="og:image" content="http://www.example.com/images/example.jpg">
	<meta property="og:image:secure_url" content="https://www.example.com/images/example.jpg">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="630">
	<!-- og main -->
	<meta property="fb:app_id" content="33333333">
	<meta property="og:title" content="Example">
	<meta property="og:description" content="description">
	<meta property="og:type" content="profile">
	<meta property="og:site_name" content="Example Parent">
	<meta property="og:locale" content="lt_LT">
	<meta property="og:locale:alternate" content="en_US">
	<meta property="og:locale:alternate" content="ru_RU">
	<meta property="og:url" content="http://www.example.com/">
	<!-- video -->
	<meta property="og:video" content="http://example.com/movie.swf" />
	<meta property="og:video:secure_url" content="https://secure.example.com/movie.swf" />
	<meta property="og:video:type" content="application/x-shockwave-flash" />
	<meta property="og:video:width" content="400" />
	<meta property="og:video:height" content="300" />
	<!-- audio -->
	<meta property="og:audio" content="http://example.com/sound.mp3" />
	<meta property="og:audio:secure_url" content="https://secure.example.com/sound.mp3" />
	<meta property="og:audio:type" content="audio/mpeg" />
	<!-- twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@mytwitter" />
	<meta name="twitter:title" content="Example" />
	<meta name="twitter:description" content="description" />
	<meta name="twitter:image" content="https://www.example.com/images/example.jpg" />
	<meta name="twitter:url" content="https://www.example.com/" />
	<!-- apple -->
	<link rel="apple-touch-icon" href="touch-icon-iphone.png"> <!-- 60 x 60 -->
	<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
	<link rel="apple-touch-startup-image" href="startup.png" /> <!-- 320 x 480 -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!-- android -->
	<link rel="shortcut icon" sizes="196x196" href="touch-icon-android-retina.png">
	<link rel="shortcut icon" sizes="128x128" href="touch-icon-android.png">
	*/ ?>
	<script>(function(w){var dpr=((w.devicePixelRatio===undefined)?1:w.devicePixelRatio);if(!!w.navigator.standalone){var r=new XMLHttpRequest();r.open('GET','/retinaimages.php?devicePixelRatio='+dpr,false);r.send()}else{document.cookie='devicePixelRatio='+dpr+'; path=/'}})(window)</script>
	<noscript><style id="devicePixelRatio" media="only screen and (-moz-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)">html{background-image:url("/retinaimages.php?devicePixelRatio=2")}</style></noscript>
	<?php
	$dev = strpos($_SERVER['HTTP_HOST'], 'upndev.com') === false;
	if ($dev) {
		require 'vendor/less-compiler/Less.php';
		require 'vendor/replacer.php';
		$content = file_get_contents('less/frontend.less');
		$parser = new Less_Parser([
			'compress'=>true,
			'sourceMap'=>true,
			'sourceMapWriteTo'=>'css/frontend.min.map',
			'sourceMapURL'=>'css/frontend.min.map',
		]);
		//$content = Replacer::get($content);
    	$parser->parse($content);
		$parser->ModifyVars([
			'dir' => '"'.__DIR__.'/../less/"',
			'url' => '"../"'
		]);
		$css = $parser->getCss();
		file_put_contents('css/frontend.min.css',$css);
	}
	?>
	<link rel="stylesheet" type="text/css" href="css/frontend.min.css">
</head>
<body>

	<header>
		<div class="wrap">
			<div class="header-input">
				<input type="text" placeholder="Paieška">
				<div class="search-click" style="background-image: url('img/glass.svg');"></div>
			</div>
			
			<div class="mobile-search" style="background-image: url('img/glass.svg');"></div>

			<div class="header-option header-option1" style="background-image: url('img/full.svg');"></div>
			<div class="header-option header-option1-on" style="background-image: url('img/full-on.svg');"></div>
			<div class="header-option header-option2" style="background-image: url('img/4boxes.svg');"></div>
			<div class="header-option header-option2-on" style="background-image: url('img/4boxes-on.svg');"></div>


			<a href="#" class="header-logo" style="background-image: url('img/logo.svg');"></a>
			
			<div class="menu-burger" style="background-image: url('img/menu-b.svg');"></div>
			<div class="add-new" style="background-image: url('img/add.svg');"></div>

			<ul class="menu">
				<li class="new"><a href="#">+ ADD NEW</a></li>
				<li class="about">
					About
					<div class="inner">
						<div class="inner-sharp"></div>
						<div class="inner-wrap">
							<ul class="about-inner">
								<li>Privacy policy</li>
								<li>Terms of use</li>
								<li>Contact</li>
							</ul>
						</div>
					</div>
				</li>
				<li class="info"><a href="#">Info</a></li>
				<li class="login"><a href="#">Login</a></li>
				<li class="logged-in"><a href="#">A.S.</a></li>
				<li class="lang">
					<a href="#">
						EN
						<div class="lang-down" style="background-image: url('img/lang-down.svg');"></div>
					</a>

					<ul class="langs">
						<li><a href="#">LT</a></li>
						<li><a href="#">RU</a></li>
						<li><a href="#">DE</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</header>