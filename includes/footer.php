	<div class="add-popup">

		<div class="popup-bg"></div>
		<div class="popup-close" style="background-image: url('img/close.svg');"></div>
		<div class="popup-content">
			<div class="popup-title">ADD NEW</div>
			<div class="popup-title-underline"></div>

			<form class="popup-box">
				<input type="text" placeholder="Name">
				<input type="text" placeholder="Subtitle">
				<input type="text" placeholder="Description">
				<select name="add-dropdown" id="add-dropdown">
					<option class="dropdown" value="" disabled selected hidden>Dropdown</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>

				<input type="submit" class="popup-button" value="Button">
			</form>
		</div>

	</div>


	<?php include 'list.php'; ?>
	<script async type="text/javascript" src="js/plugins.min.js"></script>
</body>
</html>