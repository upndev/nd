<?php include 'includes/header.php'; ?>

<section id="landing">
	<div class="landing">
		<div class="wrap">
			<h1 class="title">Title of this homepage</h1>
			<div class="title-underline"></div>
			

			<div class="single-comment">
				<div class="left-arrow">
					<div class="arrow-inner" style="background-image: url('img/arrow-left.svg');"></div>
				</div>
				<div class="right-arrow">
					<div class="arrow-inner" style="background-image: url('img/arrow-right.svg');"></div>
				</div>

				<div class="hero-comment-container">
					<div class="hero-comment-wrapper">
						
						<div class="hero-comment">
							Lorem ipsum dolor sit amet, consectetur 
							adipiscing elit. Aenean euismod bibendum laoreet.
						</div>		

					</div>
				</div>

				<div class="comment-number">1 / 1 000 0000</div>

				<div class="landing-bottom">
					<div class="left-buttons">
						<div class="bot-btn delete-b">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
							Delete
						</div>
						<div class="bot-btn edit-b">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
							Edit
						</div>
					</div>
					
					<div class="comment-faces">
						<div class="comment-face face1">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
						</div>
						<div class="comment-face face2">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
						</div>
						<div class="comment-face face3">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
						</div>
						<div class="comment-face face4">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
						</div>
						<div class="comment-face face5">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
						</div>
						<div class="comment-num">14 k.</div>
					</div>

					<div class="bot-btn share-b">
						Share
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
					</div>
				</div>
			</div>
				

			<div class="grid-comments">
				<div class="grid-wrap">
					
					<div class="grid-container" data-columns>
						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 48px; font-weight: 700">
									Lorem ipsum dolor sit amet.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 14px; font-weight: 400">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
									<br>
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
									<br>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
									<br>
									Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eo.
									<br>
									Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 14px; font-weight: 400">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
									<br>
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
									<br>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
									<br>
									Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eo.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 24px; font-weight: 700">
									Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Aenean euismod bibendum laoreet.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 24px; font-weight: 700">
									Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Aenean euismod bibendum laoreet.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 24px; font-weight: 700">
									Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Aenean euismod bibendum laoreet.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="grid-item">
							<div class="grid-item-wrap">
								
								<div class="grid-item-text" style="font-size: 24px; font-weight: 700">
									Lorem ipsum dolor sit amet, consectetur 
									adipiscing elit. Aenean euismod bibendum laoreet.
								</div>

								<div class="grid-bottom">
									<div class="grid-faces">
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/></g></g><g><g><circle class="st0" cx="327.9" cy="204.6" r="37.2"/></g></g><g><g><circle class="st0" cx="514" cy="204.6" r="37.2"/></g></g><g><g><path class="st0" d="M551.2,297.6c0,71.9-58.3,130.2-130.2,130.2s-130.2-58.3-130.2-130.2h-37.2c0,92.5,75,167.4,167.4,167.4s167.4-75,167.4-167.4H551.2z"/></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M327.9,204.6c20.5,0,37.2,16.7,37.2,37.2h37.2c0-41.1-33.3-74.4-74.4-74.4c-41.1,0-74.4,33.3-74.4,74.4h37.2C290.7,221.3,307.4,204.6,327.9,204.6z"/><path class="st0" d="M514,167.4c-41.1,0-74.4,33.3-74.4,74.4h37.2c0-20.5,16.7-37.2,37.2-37.2c20.5,0,37.2,16.7,37.2,37.2h37.2C588.4,200.7,555.1,167.4,514,167.4z"/><path class="st0" d="M551.2,297.6H290.7c-10.3,0-18.6,8.3-18.6,18.6c0,82.2,66.6,148.8,148.8,148.8s148.8-66.6,148.8-148.8C569.8,306,561.4,297.6,551.2,297.6z M420.9,427.9c-54.5,0-101-39.3-110.1-93h220.3C522,388.6,475.4,427.9,420.9,427.9z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><rect x="290.7" y="353.4" class="st0" width="260.4" height="37.2"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><path class="st0" d="M382.3,248.9c2.9-7,1.3-15-4.1-20.3l-74.4-74.4l-26.2,26.4l42.8,42.6h-48.2v37.2h93C372.6,260.4,379.4,255.8,382.3,248.9z"/><path class="st0" d="M564.4,180.6L538,154.2l-74.4,74.4c-7.3,7.2-7.3,19-0.1,26.3c3.5,3.5,8.3,5.5,13.3,5.5h93v-37.2h-48.2L564.4,180.6z"/><path class="st0" d="M495.4,316.2H346.5c-41.1,0-74.4,33.3-74.4,74.4v37.2c0,10.3,8.3,18.6,18.6,18.6h260.4c10.3,0,18.6-8.3,18.6-18.6v-37.2C569.8,349.6,536.4,316.2,495.4,316.2z M532.6,409.3H309.3v-18.6c0-20.5,16.7-37.2,37.2-37.2h148.8c20.5,0,37.2,16.7,37.2,37.2V409.3z"/></g></g></g></svg>
										</div>
										<div class="grid-face">
											<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><g><path class="st0" d="M420.9,0C256.6,0,123.3,133.3,123.3,297.6c0,164.4,133.3,297.6,297.6,297.6S718.6,462,718.6,297.6C718.6,133.3,585.3,0,420.9,0z M420.9,558.1c-143.8,0-260.4-116.6-260.4-260.4S277.1,37.2,420.9,37.2c143.8,0,260.4,116.6,260.4,260.4S564.8,558.1,420.9,558.1z"/><circle class="st0" cx="327.9" cy="204.6" r="37.2"/><circle class="st0" cx="514" cy="204.6" r="37.2"/><path class="st0" d="M420.9,279c-92.5,0-167.4,75-167.4,167.4h37.2c0-71.9,58.3-130.2,130.2-130.2s130.2,58.3,130.2,130.2h37.2C588.4,354,513.4,279,420.9,279z"/></g></g></g></svg>
										</div>
										<div class="grid-num">14 k.</div>
									</div>
									<div class="grid-buttons">
										<div class="grid-del">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="207.9 0 426.1 595.3" style="enable-background:new 207.9 0 426.1 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="233" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="508.7" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><rect x="370.8" y="213" class="st0" width="100.3" height="382.2"/></g></g><g><g><path class="st0" d="M527.5,75.2V0h-213v75.2H207.9v100.3H634V75.2H527.5z M489.9,75.2H352V37.6h137.9V75.2z"/></g></g></svg>
										</div>
										<div class="grid-edit">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 0 595.3 595.3" style="enable-background:new 123.3 0 595.3 595.3;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><rect x="190.3" y="257.5" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 891.519 301.0912)" class="st0" width="386.3" height="155.3"/></g></g><g><g><rect x="541.7" y="79.8" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1069.2773 -128.0112)" class="st0" width="38.8" height="155.3"/></g></g><g><g><polygon class="st0" points="171.4,451.2 123.3,595.3 267.4,547.2 "/></g></g><g><g><rect x="589.6" y="7.9" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 1141.1084 -301.4936)" class="st0" width="86.8" height="155.3"/></g></g></svg>
										</div>
										<div class="grid-share">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="123.3 42.1 595.3 511.1" style="enable-background:new 123.3 42.1 595.3 511.1;" xml:space="preserve"><style type="text/css">.st0{fill:#100071;}</style><g><g><path class="st0" d="M718.6,294.2L466.5,42.1V210C277,210,123.3,363.6,123.3,553.2c69.6-126.9,207.8-192.4,343.2-175.6v168.6L718.6,294.2z"/></g></g></svg>
										</div>
									</div>
								</div>

							</div>
						</div>
						

					</div>

				</div>
			</div>

		</div>
	</div>
</section>

<?php include 'includes/footer.php'; ?>