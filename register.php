<?php include 'includes/header.php'; ?>

<section id="register">
	<div class="register">
		<div class="wrap">
			<h1 class="title">Register</h1>
			<div class="title-underline"></div>

			<form class="reg-box">
				<input type="text" placeholder="Name">
				<input type="text" placeholder="Email">
				<input type="password" placeholder="Password">

				<select name="dropdown" id="dropdown">
					<option class="dropdown" value="" disabled selected hidden>Dropdown</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>

				<div class="reg-bottom">
					<div class="terms">By continuing, you agree to our<br>
						<a href="#">TERMS OF SERVICE</a>  and  <a href="#">PRIVACY POLICY</a></div>
					<input type="submit" class="reg-b" value="Register">
					<a href="#" class="have-acc">Have an account? <span>LOG IN</span></a>
				</div>
			</form>
			
		</div>
	</div>
</section>

<?php include 'includes/footer.php'; ?>